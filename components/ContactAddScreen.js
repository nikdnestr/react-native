import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export const ContactAddScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.form}>
        <Text>Name</Text>
        <TextInput style={styles.input}></TextInput>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
  },
  form: {
    width: '90%',
  },
  input: {
    borderWidth: 1,
  },
});
