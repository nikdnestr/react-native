import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  SafeAreaView,
  View,
  ScrollView,
  Image,
  Button,
} from 'react-native';
import * as Contacts from 'expo-contacts';

export const ContactsScreen = ({ navigation }) => {
  const [contacts, setContacts] = React.useState([]);
  const [search, setSearch] = React.useState('');
  const [placeholder, setPlaceholder] = React.useState('type contact name');

  React.useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({});

        if (data.length > 0) {
          setContacts(data);
        }
        console.log(contacts);
      }
    })();
  }, []);

  const goTo = (data) => {
    navigation.navigate('ContactDetailScreen', data);
    setPlaceholder('type contact name');
  };

  const onSearch = () => {
    for (let i = 0; i < contacts.length; i++) {
      const contact = contacts[i];
      if (contact.name.toLowerCase() === search.toLowerCase()) {
        goTo(contact);
        break;
      }
      setPlaceholder('invalid name');
    }
    setSearch('');
  };

  const createContact = () => {
    console.log(contacts);
    if (contacts.length > 0) {
      return contacts.map((i) => {
        let image =
          'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-contact-512.png';
        if (i.imageAvailable) {
          image = i.image.uri;
        }
        return (
          <View style={styles.contactContainer} key={i.id}>
            <Image
              style={styles.avatar}
              source={{
                uri: image,
              }}
            />
            <View>
              <Text style={styles.contactText}>{i.firstName}</Text>
              <Text style={styles.contactText}>{i.phoneNumbers[0].number}</Text>
            </View>
            <View style={styles.buttonContainer}>
              <Button color="black" title={'More'} onPress={() => goTo(i)} />
            </View>
          </View>
        );
      });
    }
    return (
      <View>
        <Text>no contacts</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <TextInput
        style={styles.input}
        value={search}
        placeholder={placeholder}
        onChangeText={setSearch}
        onSubmitEditing={() => onSearch(search)}
      />
      <ScrollView style={styles.contacts}>{createContact()}</ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    padding: 10,
    marginTop: 10,
    width: '80%',
    color: 'black',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: 'gray',
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50,
    marginRight: 10,
  },
  contactContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: 'gray',
    padding: 10,
    borderRadius: 10,
  },
  contactText: {
    color: 'white',
  },
  contacts: {
    flex: 1,
    height: 10,
    width: '80%',
  },
  buttonContainer: {
    marginLeft: 'auto',
  },
});
