import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import * as Linking from 'expo-linking';

export const ContactDetailScreen = ({ navigation, route }) => {
  console.log(route.params);
  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        source={{
          uri: route.params.imageAvailable
            ? route.params.image.uri
            : 'https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-contact-512.png',
        }}
      />
      <View>
        <Text style={styles.text}>Name: {route.params.name}</Text>
        <Text style={styles.text}>
          Number: {route.params.phoneNumbers[0].number}
        </Text>
      </View>
      <Button
        title="call"
        color="black"
        onPress={() =>
          Linking.openURL(`tel:${route.params.phoneNumbers[0].number}`)
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  avatar: {
    height: 200,
    width: 200,
    borderRadius: 100,
  },
  text: {
    marginBottom: 15,
  },
});
