import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ContactsScreen } from './components/ContactsScreen';
import { ContactDetailScreen } from './components/ContactDetailScreen';
import { ContactAddScreen } from './components/ContactAddScreen';

export default function App() {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Contacts" component={ContactsScreen} />
        <Stack.Screen
          name="ContactDetailScreen"
          component={ContactDetailScreen}
        />
        <Stack.Screen name="ContactAddScreen" component={ContactAddScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
